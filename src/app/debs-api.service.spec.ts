import { TestBed, inject } from '@angular/core/testing';

import { DebsApiService } from './debs-api.service';

describe('DebsApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DebsApiService]
    });
  });

  it('should be created', inject([DebsApiService], (service: DebsApiService) => {
    expect(service).toBeTruthy();
  }));
});
