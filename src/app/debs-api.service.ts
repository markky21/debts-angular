import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';

interface Debt {
  Id: number;
  Name: string;
  NIP: number;
  Value: number;
  Address: string;
  DocumentType: string;
  Price: number;
  Number: number;
}

@Injectable()
export class DebsApiService {

  private searchDebs = new Subject();
  private currentDebt = new Subject();
  private newSearch = new BehaviorSubject(true);

  debsApiUrl = 'http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/';

  constructor(private http: Http) {
  }

  getNewSearchObservable() {
    return this.newSearch.asObservable();
  }

  setNewSearch(status: boolean) {
    this.newSearch.next(status);
  }

  getCurrentDebtObservable(): Observable<Debt> | null {
    return this.currentDebt.asObservable();
  }

  setCurrentDebt(debt: Debt) {
    this.currentDebt.next(debt);
  }

  getDebsObservable(): Observable<[Debt]> {
    return this.searchDebs.asObservable();
  }

  getDebtsCount() {
    return this.http.get(this.debsApiUrl + 'GetDebtsCount')
      .map(response => response.json());
  }

  getTopDebs() {
    this.http.get(this.debsApiUrl + 'GetTopDebts')
      .map(response => response.json())
      .subscribe(respond => {
        this.searchDebs.next(respond);
      }, error => {
        console.log(error);
      });
  }

  getFilteredDebts(query: string) {

    this.setNewSearch(true);
    const options = new Object();
    options['headers'] = {
      'Content-Type': 'application/json'
    };

    this.http.post(this.debsApiUrl + 'GetFilteredDebts', JSON.stringify(query), options).map(response => response.json())
      .subscribe(respond => {
        // setTimeout(() => { // symulacja długiego czasu wyszukiwania
          this.searchDebs.next(respond);
        // }, 1000);

      }, error => {
        console.log(error);
      });
  }

}
