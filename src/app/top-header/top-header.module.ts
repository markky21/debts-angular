import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {TopHeaderComponent} from './top-header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  declarations: [
    TopHeaderComponent,
  ],
  exports: [
    TopHeaderComponent
  ]
})
export class TopHeaderModule {
}
