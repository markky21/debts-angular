import {Component, OnInit} from '@angular/core';
import {DebsApiService} from '../debs-api.service';

import {listAnimationBottom, listAnimationRight} from '../_animations/listAnimation';

@Component({
  selector: 'app-top-header',
  template: `
    <header class="main-header">
      <div class="container">
        <div class="row">
          <div class="main-header__search col-12 col-lg-6">
            <section class="debs-search" @listAnimationBottom>
              <div class="debs-search__info">Podaj numer sprawy, nazwę lub nip dłużnika</div>
              <form class="debs-search__form" #formRef="ngForm" (ngSubmit)="searchDebs(formRef)">
                <input type="text" class="debs-search__form-input" name="search-input" required minlength="3" #searchRef="ngModel" ngModel/>
                <button class="debs-search__form-submit" type="submit">szukaj</button>
                <label class="warning" *ngIf="searchRef.invalid && showWarning">
                  <span *ngIf="searchRef.errors?.required" @listAnimationRight>Wyszukiwana fraza nie może być pusta</span>
                  <span *ngIf="searchRef.errors?.minlength" @listAnimationRight>Wyszukiwana fraza musi mieć min. 3 znaki</span>
                </label>
              </form>
            </section>
          </div>
          <div class="main-header__result col-12 col-lg-6" *ngIf="debtsCount" @listAnimationRight>
            <section class="debs-summary">
              <div class="debs-summary__info">całkowita ilość spraw</div>
              <data class="debs-summary__data" value="88">{{debtsCount}}</data>
            </section>
          </div>
        </div>
      </div>
    </header>
  `,
  styleUrls: ['./top-header.component.scss'],
  animations: [listAnimationBottom, listAnimationRight]
})

export class TopHeaderComponent implements OnInit {

  showWarning = false;
  debtsCount = 0;

  constructor(private debsApiService: DebsApiService) {
  }

  ngOnInit() {

    this.getDebtsCount();
    this.debsApiService.getTopDebs();
  }

  getDebtsCount() {
    this.debsApiService.getDebtsCount().subscribe(debs => {
      this.debtsCount = debs + 1; // nie lubie mieć trzech '6' na ekranie ;)
    });
  }

  searchDebs(formRef) {
    console.log(formRef);

    if (!formRef.valid) {
      this.showWarning = true;
      return;
    }
    this.showWarning = false;

    const query = formRef.controls['search-input'].value;
    this.debsApiService.getFilteredDebts(query);
  }

}
