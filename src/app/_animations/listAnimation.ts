import {trigger, style, transition, animate} from '@angular/animations';

export const listAnimationLeft = trigger('listAnimationLeft', [

  transition(':enter', [

    style({
      opacity: 0,
      transform: 'translateX(-100px)'
    }),
    animate('300ms ease-out', style({
      opacity: 1,
      transform: 'translateX(0)'
    }))

  ]),

  transition(':leave', [

    style({
      opacity: 1,
      transform: 'translateX(0)'
    }),
    animate('300ms ease-out', style({
      opacity: 0,
      transform: 'translateY(-100px)'
    }))

  ])

]);

export const listAnimationRight = trigger('listAnimationRight', [

  transition(':enter', [

    style({
      opacity: 0,
      transform: 'translateX(30px)'
    }),
    animate('300ms ease-out', style({
      opacity: 1,
      transform: 'translateX(0)'
    }))

  ]),

  transition(':leave', [

    style({
      opacity: 1,
      transform: 'translateX(0)'
    }),
    animate('300ms ease-out', style({
      opacity: 0,
      transform: 'translateY(30px)'
    }))

  ])

]);

export const listAnimationBottom = trigger('listAnimationBottom', [

  transition(':enter', [

    style({
      opacity: 0,
      transform: 'translateY(30px)'
    }),
    animate('300ms ease-out', style({
      opacity: 1,
      transform: 'translateY(0)'
    }))

  ]),

  transition(':leave', [

    style({
      opacity: 1,
      transform: 'translateY(0)'
    }),
    animate('300ms ease-out', style({
      opacity: 0,
      transform: 'translateY(30px)'
    }))

  ])

]);
