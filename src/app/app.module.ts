import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';

import {TableModule} from './table/table.module';
import {TopHeaderModule} from './top-header/top-header.module';

import {AppComponent} from './app.component';

import {DebsApiService} from './debs-api.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    TableModule,
    TopHeaderModule,
  ],
  providers: [DebsApiService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
