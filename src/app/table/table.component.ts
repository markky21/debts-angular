import {Component, OnInit} from '@angular/core';

import {DebsApiService} from '../debs-api.service';
import {listAnimationBottom, listAnimationLeft} from '../_animations/listAnimation';


interface Debt {
  Id: number;
  Name: string;
  NIP: number;
  Value: number;
  Address: string;
  DocumentType: string;
  Price: number;
  Number: number;
}

@Component({
  selector: 'app-table',
  template: `
    <div class="container">
      <div class="row">
        <div class="col-12">

          <section class="main-loader">
            <img src="/assets/svg/Loader.svg" alt="" *ngIf="newSearch" @listAnimationBottom
                 (@listAnimationBottom.done)="mainLoaderAnimation($event)">
          </section>

          <section class="table-debs" @listAnimationBottom>
            <header class="table-debs__header row">
              <div class="col-5 hidden-sm-down table-debs__debtor">Dłużnik</div>
              <div class="col-3 hidden-sm-down table-debs__nip">Nip</div>
              <div class="col-3 hidden-sm-down table-debs__value">Kwota zadłużenia</div>
              <div class="col-1 hidden-sm-down table-debs__additional"></div>
            </header>

            <app-table-row *ngFor="let debt of displayedDebs" [debt]="debt"></app-table-row>

          </section>

        </div>
      </div>
    </div>
  `,
  styleUrls: ['./table.component.scss'],
  animations: [
    listAnimationLeft,
    listAnimationBottom,
  ]
})

export class TableComponent implements OnInit {

  debs: [Debt];
  newSearch: Boolean = true;
  animationDone: Boolean = false;
  allDebsShowed: Boolean = false;
  displayedDebs: Array<Debt> = [];

  constructor(private debsApiService: DebsApiService) {
    this.debsApiService.getDebsObservable().subscribe(debs => {
      this.debs = debs;
      this.debsApiService.setNewSearch(false);
      this.showNewDebs();
    });

    this.debsApiService.getNewSearchObservable().subscribe(status => {
      this.displayedDebs = [];
      this.newSearch = status;
      this.animationDone = false;
    });
  }

  ngOnInit() {
  }

  mainLoaderAnimation($event) {
    if ($event.toState === 'void') {
      this.animationDone = true;
    }
  }

  showNewDebs() {
    let i = 0;
    this.allDebsShowed = false;
    const pushNextDebt = () => {
      if (i < this.debs.length) {
        setTimeout(() => {
          this.displayedDebs.push(this.debs[i]);
          i++;
          pushNextDebt();
        }, 40);
      } else {
        this.allDebsShowed = true;
      }
    };
    pushNextDebt();
  }

}
