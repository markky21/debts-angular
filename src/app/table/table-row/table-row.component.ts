import {Component, OnInit, Input} from '@angular/core';
import {listAnimationLeft} from '../../_animations/listAnimation';
import {DebsApiService} from '../../debs-api.service';

interface Debt {
  Id: number;
  Name: string;
  NIP: number;
  Value: number;
  Address: string;
  DocumentType: string;
  Price: number;
  Number: number;
}

@Component({
  selector: 'app-table-row',
  template: `
    <article class="table-debs__record-row" [ngClass]="{'active': currentDebt?.Id === debt.Id}" @listAnimationLeft>
      <div class="row">
        <div class="col-12 col-md-5 table-debs__debtor">
          <label class="table-debs__record-title">Dłużnik</label>
          <data class="table-debs__record-value" value="{{debt.Name}}">{{debt.Name}}</data>
        </div>
        <div class="col-12 col-md-3 table-debs__nip">
          <label class="table-debs__record-title">NIP</label>
          <data class="table-debs__record-value" value="{{debt.NIP}}">{{debt.NIP}}</data>
        </div>
        <div class="col-12 col-md-3 table-debs__value">
          <label class="table-debs__record-title">Kwota zadłużenia</label>
          <data class="table-debs__record-value" value="{{debt.Value}}">{{debt.Value}} zł</data>
        </div>
        <div class="col-12 col-md-1 table-debs__additional">
          <span class="table-debs__show-more" (click)="setCurrentDebt()">{{showMoreText}}</span>
        </div>
      </div>
      <div class="row table-debs__active-row">
        <div class="col-12 col-md-5 table-debs__address">
          <label class="table-debs__record-title">Adres</label>
          <data class="table-debs__record-value" value="{{debt.Address}}">{{debt.Address}}</data>
        </div>
        <div class="col-12 col-md-3 table-debs__nip">
          <label class="table-debs__record-title">Rodzaj/typ dokumentu stanowiący podstawę dla wierzytelności</label>
          <data class="table-debs__record-value" value="{{debt.DocumentType}}">{{debt.DocumentType}}</data>
        </div>
        <div class="col-12 col-md-2 table-debs__value">
          <label class="table-debs__record-title">Cena zadłużenia</label>
          <data class="table-debs__record-value" value="{{debt.Value}}">{{debt.Price}} zł</data>
        </div>
        <div class="col-12 col-md-2 table-debs__number">
          <div class="table-debs__number-wrapper">
            <label class="table-debs__record-title">Numer</label>
            <data class="table-debs__record-value" value="{{debt.Number}}">{{debt.Number}}</data>
          </div>
        </div>
      </div>
    </article>
  `,
  styleUrls: ['./table-row.component.scss'],
  animations: [listAnimationLeft]
})
export class TableRowComponent implements OnInit {

  @Input() debt: Debt;
  currentDebt: Debt;
  showMoreText: string = 'Więcej';

  constructor(private debsApiService: DebsApiService) {
  }

  ngOnInit() {
    this.debsApiService.getCurrentDebtObservable()
      .subscribe(debt => {
        this.currentDebt = debt;
        this.showMoreText = debt && debt.Id === this.debt.Id ? 'Mniej' : 'Więcej';
      });
  }

  setCurrentDebt() {
    if (this.currentDebt && this.currentDebt.Id === this.debt.Id) {
      this.debsApiService.setCurrentDebt(null);
    } else {
      this.debsApiService.setCurrentDebt(this.debt);
    }
  }

}
